import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: 'http://35.194.195.90/auth/',
    })
}