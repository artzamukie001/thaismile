import axios from 'axios'
import { state } from '../store'

export default () => {
    console.debug(state);
    return axios.create({
        baseURL: 'http://35.194.195.90/',
        headers: {
            Authorization: `Bearer ${state().token}`
        }
    })
}