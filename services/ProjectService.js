import Api from './Api'
import qs from 'qs';

const requestConfig = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}

export default {
    // Project Service
    getAll (){
        return Api().get('/projects')
    },
    getData () {
        return Api().get(`project/`)
    },
    addData (data) {
       return Api().post('/project', data)
   },
   updateData (id,data) {
    return Api().put(`/project/${id}`,data)
    },
    deleteData (id) {
        return Api().delete(`/project/${id}`)
    },

    // Feature Service
    getAllFeature(id) {
        return Api().get(`/project/${id}/feature`)
    },
    addFeature (feature) {
        return Api().post(`/feature`, feature)
    },
    updateFeature (id,feature) {
     return Api().put(`/feature/${id}`,feature)
     },
    deleteFeature (id) {
        return Api().delete(`/feature/${id}`)
    },

    // Test Case Service
    getAllTestcase(id,project_id){
        return Api().get(`/project/${project_id}/feature/${id}/testcase`)
    },
    addTestcase(testCase){
        return Api().post(`/testcase`,testCase)
    },
    updateTestcase (id,testCase) {
        return Api().put(`/testcase/${id}`,testCase)
    },
    deleteTestcase (id) {
        return Api().delete(`/testcase/${id}`)
    },

    //TEP
    getAllTep(id) {
        return Api().get(`/project/${id}/teps`)
    },
    
    addTep(id,tep) {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
        return Api().post(`/project/${id}/tep`,tep,config)
    },

    updateTep (id,tep) {
        return Api().put(`/project/${id}/tep`,tep)
    },

    getfeaturetep(id,tep){
        return Api().get(`/TEP/feature/${id}`,tep)
    },
    deleteTep (id) {
        return Api().delete(`/project/${id}/tep`,tep)
    },

    //Feature in TEP
    getAllfeatureinTEP(projectId,id) {
        return Api().get(`/project/${projectId}/TEP/${id}/feature`)
    },
    addFeatureinTeP(projectId,id,feature){
        const featureQs = qs.stringify(feature);
        return Api().post(`/project/${projectId}/TEP/${id}/feature`,featureQs,requestConfig)
    },

    getAllTestcaseToTep(project_id,id,tep_id){
        return Api().get(`/project/${project_id}/TEP/${tep_id}/feature/${id}/testcase`)
    },

    addTestcaseToFeature(project_id,id,tep_id,TestCase){
        const testcaseQs = qs.stringify(TestCase, { indices: false })
        return Api().post(`/project/${project_id}/TEP/${tep_id}/feature/${id}/testcase`,testcaseQs,requestConfig)
    }
}