
export const state = () => {
    return{
        token:localStorage.getItem('access_token') || '',
        user:{},
        isLoggedIn:false
    }
}

export const mutations = {
    setToken (state,token){
        console.log('how to keep token');
        
        localStorage.setItem('access_token', token)
        state.isLoggedIn = !!(token)
    },
    setUser (state,user){
        state.user = user
    }
}

export const actions = {
    setToken ({commit},token){
        commit('setToken',token)
        console.log('akjf;lasjdf');
        
    },
    setUser ({commit},user){
        commit('setUser',user)
    }
   
}
export const getters = {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    user: state => state.user
}

