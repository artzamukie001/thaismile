
export default function ({ store, redirect }) {

    // If the user is not authenticated
    console.log(store.state);
    if (!store.state.token || !store.state.user) {
      return redirect('/login')
    }
  }